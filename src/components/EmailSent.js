import React, { useState, createRef, useEffect } from 'react';
import lottie from 'lottie-web';

const EmailSent = () => {
  const [animComplete, setAnimComplete] = useState(false);
  let animationContainer = createRef();

  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animationContainer.current,
      renderer: 'svg',
      autoplay: true,
      loop: false,
      path: 'https://assets7.lottiefiles.com/packages/lf20_y9qOnk.json',
    });
    anim.addEventListener('complete', () => {
      anim.destroy();
      setAnimComplete(true);
    });
  }, [animationContainer]);

  return (
    <div className='emailSent'>
      {!animComplete && (
        <div className='emailAnimation' ref={animationContainer}></div>
      )}
      {animComplete && <h3>Email sent!</h3>}
    </div>
  );
};

export default EmailSent;
