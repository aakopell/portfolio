import React, { useState } from 'react';
import '../styles/Contact.css';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import emailjs from '@emailjs/browser';

const ContactForm = ({ setEmailSent }) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [invalidEmail, setInvalidEmail] = useState(false);
  const [emailBorder, setEmailBorder] = useState('');
  const [nameBorder, setNameBorder] = useState('');
  const [messageBorder, setMessageBorder] = useState('');

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      if (name && email && message) {
        if (isValidEmail(email)) {
          const serviceId = 'service_2rd9pec';
          const templateId = 'template_9h3d9rc';
          const userId = 'user_eGWQnZCPzczHXKcn0VXSY';
          const templateParams = {
            name,
            email,
            message,
          };
          await emailjs.send(serviceId, templateId, templateParams, userId);

          setName('');
          setEmail('');
          setMessage('');
          setEmailSent(true);
        } else {
          setInvalidEmail(true);
          setEmailBorder('border-warning');
        }
      } else {
        if (!email) {
          setEmailBorder('border-warning');
        }
        if (!name) {
          setNameBorder('border-warning');
        }
        if (!message) {
          setMessageBorder('border-warning');
        }
      }
    } catch (error) {
      alert('Not able to send email');
    }
  };

  const isValidEmail = (emailAddress) => {
    const regex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(emailAddress).toLowerCase());
  };

  return (
    <Form>
      <Form.Group controlId='formBasicName' className='mb-2'>
        <Form.Label>Your Name</Form.Label>
        <Form.Control
          type='name'
          placeholder='Enter Name'
          value={name}
          onChange={(e) => {
            setName(e.target.value);
            setNameBorder('');
          }}
          className={nameBorder}
        />
        {nameBorder && (
          <Form.Text className='text-muted'>Please enter your name</Form.Text>
        )}
      </Form.Group>
      <Form.Group controlId='formBasicEmail' className='mb-2'>
        <Form.Label>Your Email</Form.Label>
        <Form.Control
          type='email'
          placeholder='Enter Email'
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
            setInvalidEmail(false);
            setEmailBorder('');
          }}
          className={emailBorder}
        />
        {invalidEmail && (
          <Form.Text className='text-muted'>Please use a valid email</Form.Text>
        )}
        {emailBorder && !invalidEmail && (
          <Form.Text className='text-muted'>Please enter your email</Form.Text>
        )}
      </Form.Group>
      <Form.Group controlId='formBasicMessage' className='mb-2'>
        <Form.Label>Message</Form.Label>
        <Form.Control
          className={messageBorder}
          type='message'
          placeholder='Message'
          as='textarea'
          rows={3}
          value={message}
          onChange={(e) => {
            setMessage(e.target.value);
            setMessageBorder('');
          }}
        />
        {messageBorder && (
          <Form.Text className='text-muted'>
            Please enter your a message
          </Form.Text>
        )}
      </Form.Group>
      <div className='emailButton'>
        <Button type='submit' onClick={handleSubmit}>
          Send Email
        </Button>
      </div>
    </Form>
  );
};

export default ContactForm;
