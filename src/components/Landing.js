import React from 'react';
import '../styles/Landing.css';
import { LandingHelper } from '.';

const Landing = () => {
  return (
    <div className='background'>
      <div className='landingScreen'>
        <LandingHelper />
      </div>
    </div>
  );
};

export default Landing;
