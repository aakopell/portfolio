import React, { useEffect, useState } from 'react';
import '../styles/Navbar.css';

const Navbar = () => {
  const [aboutFocus, setAboutFocus] = useState('');
  const [projectsFocus, setProjectsFocus] = useState('');
  const [contactFocus, setContactFocus] = useState('');

  useEffect(() => {
    const about = document.querySelector('#about');
    const projects = document.querySelector('#projects');
    const contact = document.querySelector('#contact');

    window.addEventListener('scroll', () =>
      handleScroll(
        about.offsetTop - 100,
        projects.offsetTop - 100,
        contact.offsetTop - 100
      )
    );
  });

  const handleScroll = (aboutTop, projectsTop, contactTop) => {
    if (window.scrollY < aboutTop) {
      if (aboutFocus === 'active' || contactFocus === 'active') {
        setAboutFocus('notActive');
        setProjectsFocus('notActive');
        setContactFocus('notActive');
      }
    } else if (window.scrollY >= aboutTop && window.scrollY < projectsTop) {
      if (aboutFocus !== 'active') {
        setAboutFocus('active');
        setProjectsFocus('notActive');
        setContactFocus('notActive');
      }
    } else if (window.scrollY >= projectsTop && window.scrollY < contactTop) {
      if (projectsFocus !== 'active') {
        setAboutFocus('notActive');
        setProjectsFocus('active');
        setContactFocus('notActive');
      }
    } else {
      if (contactFocus !== 'active') {
        setAboutFocus('notActive');
        setProjectsFocus('notActive');
        setContactFocus('active');
      }
    }
  };

  return (
    <div id='navbar'>
      <a href='#about' className={aboutFocus}>
        About
      </a>
      <a href='#projects' className={projectsFocus}>
        Projects
      </a>
      <a href='#contact' className={contactFocus}>
        Contact
      </a>
    </div>
  );
};

export default Navbar;
