import React, { useState } from 'react';
import { ContactForm, EmailSent } from '.';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import { SocialIcon } from 'react-social-icons';

const Contact = () => {
  const [emailSent, setEmailSent] = useState(false);

  return (
    <Container className='section p-3' id='contact'>
      <h1>Contact Me</h1>
      <div className='contactContainer'>
        <div className='emailWrapper'>
          <div className='emailContainer'>
            {!emailSent && <ContactForm setEmailSent={setEmailSent} />}
            {emailSent && <EmailSent />}
          </div>
        </div>
        <Row className='justify-content-center'>
          <SocialIcon
            className='socialIcon'
            url='https://www.linkedin.com/in/adamkopell/'
          />
          <SocialIcon className='socialIcon' url='https://github.com/akopell' />
        </Row>
      </div>
    </Container>
  );
};

export default Contact;
